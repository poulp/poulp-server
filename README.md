# Poulp server

A web app build with Valum to manage and explore a Poulp repository.

## Building front-end

```bash
npm install -g gulp-cli # First time only
gulp fonts
gulp images
gulp sass
gulp watch # Only if you plan to modify the front
```

## Building back-end

```bash
sudo poulp install
poulp build
./build/poulp-server
```

Then go on [`http://localhost:3003`](http://localhost:3003)! :tada:

## Architecture of the back end

The `src` folder contains all the back-end code. It is divided like that:

- `main.vala`, the entry point of the programm and some programm-wide useful methods ;
- `compose.vala`, custom extensions to Compose ;
- `views`, with various method that renders a specific view ;
- `controllers`, where all the logic of the app lives.
