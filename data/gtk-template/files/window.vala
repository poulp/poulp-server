using Gtk;

public class MyWindow : Window {

    public MyWindow () {
        this.title = "GTK+3.0 window";
        this.window_position = WindowPosition.CENTER;
        set_default_size (800, 400);
    }
}