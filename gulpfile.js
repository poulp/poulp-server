const gulp = require ('gulp')
const sass = require('gulp-sass')
const path = require('path')
const fontAwesome = require('node-font-awesome')
const watch = require('gulp-watch')
var concat = require('gulp-concat')
var minifyCSS = require('gulp-minify-css')
var rename = require('gulp-rename')

gulp.task('fonts', function() {
  gulp.src(fontAwesome.fonts)
    .pipe(gulp.dest('./static/fonts'))
  gulp.src(fontAwesome.css)
    .pipe(gulp.dest('./static/css'))
})

gulp.task('images', function() {
  gulp.src('./assets/images/*.*')
    .pipe(gulp.dest('./static/img/'))
})

gulp.task('sass', function () {
  gulp.src('./assets/style/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('./static/css'))
})

gulp.task('watch', function () {
  gulp.watch('./assets/style/*.scss', ['sass'])
})

gulp.task('default', [ 'watch' ])
