using Gee;

namespace Compose {

    public delegate string ForeachCallback<T> (T item, int index);

    public string for_each<G> (ArrayList<G> list, owned ForeachCallback<G> callback, string fallback = "") {
        string res = "";
        int i = 0;
        foreach (G item in list) {
            res += callback (item, i);
            i++;
        }
        if (res == "") {
            res = fallback;
        }
        return res;
    }

    public delegate bool? FilterFunc<T> (T item, int item_count);

    public ArrayList<G> filter<G> (ArrayList<G> original, owned FilterFunc<G> callback) {
        ArrayList<G> res = new ArrayList<G> ();
        foreach (G item in original) {
            bool? include = callback (item, res.size);
            if (include != null && include) {
                res.add (item);
            } else if (include == null) {
                break;
            }
        }
        return res;
    }

    public delegate string LoopFunc (int index);

    public string loop (int i, int max, LoopFunc f) {
        var res = "";
        for (; i < max; i++) {
            res += f (i);
        }
        return res;
    }
}
