using Valum;
using Valum.ContentNegotiation;
using VSGI;
using Poulp;
using Gee;
using Views;
using Compose;
using Compose.HTML5;

public class HomeController : Object {

    private Repository repo;

    private ArrayList<Manifest> packages;

    const int PKG_PER_LINE = 4;

    public HomeController.on (Router app, Repository repo, ArrayList<Manifest> packages) {
        this.repo = repo;
        this.packages = packages;
        app.get ("/", accept ("text/html", home));
        app.get ("/about", accept ("text/html", about));
    }

    public bool home (Request req, Response res) throws Error {
        var trending_tags = new ArrayList<string>.wrap ({ "poulp", "web", "toml", "json", "ia", "gtk", "flatpak" });

        return res.expand_utf8 (template (repo.name,
            string.join ("",
                h2 ({}, "Trending packages"),
                div ({"class=packages"},
                    loop (0, 6, (i) => {
                        if (packages.size > i) {
                            return card (packages[i]);
                        } else {
                            return div ({"class=bg-msg-center"},
                                p ({}, "No other trending package…")
                            );
                        }
                    })
                ),

                h2 ({}, "Trending tags"),
                div ({},
                    ul ({"class=tags spaced"},
                        for_each<string> (trending_tags, (tag, i) => {
                            return li ({}, a ("/search?q=#%s".printf (tag), {}, tag));
                        })
                    )
                ),

                h3 ({}, "Want to see your package here?"),
                div ({},
                    a ("/new", {"class=button"}, "Publish it!")
                ),

                h2 ({}, "Recently updated"),
                div ({"class=packages"},
                    loop (0, 6, (i) => {
                        if (packages.size > i) {
                            return card (packages[i]);
                        } else {
                            return div ({"class=bg-msg-center"},
                                p ({}, "No other trending package…")
                            );
                        }
                    })
                ),

                h2 ({}, "Setup this repository"),
                div ({},
                    p ({}, "Just run this command."),
                    element ("pre", {}, code ({}, "poulp repo add http://foo.bar"))
                )
            )
        ));
    }

    private string card (Manifest pkg) {
        return div ({},
            h3 ({},
                a (@"/$(pkg.name)", {}, pkg.name),
                span ({"class=light"}, " &mdash; " + pkg.version.to_string ())
            ),
            p ({"class=description"}, pkg.description),
            ul ({"class=tags"},
                for_each<string> (pkg.keywords, (tag, i) => {
                    return li ({}, a ("/search?q=#%s".printf (tag), {}, tag));
                })
            )
        );
    }

    public bool about (Request req, Response res) throws Error {
        return res.expand_utf8 (template ("About",
            Compose.HTML5.main ({},
                h1 ({}, "About"),
                p ({}, "poulp is a package manager for Vala. It let you install packages from distant repositories, like this one."),
                p ({}, "blablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla"),
                p ({}, "blablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla"),
                p ({}, "blablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla")
            )
        ));
    }
}
