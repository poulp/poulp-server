using Valum;
using Valum.ContentNegotiation;
using VSGI;
using Poulp;
using Gee;
using Views;
using Compose;
using Compose.HTML5;

public class PackagesController : Object {

    private Repository repo;

    private ArrayList<Manifest> packages;

    private ArrayList<string> pending_packages = new ArrayList<string> ();

    public PackagesController.on (Router app, Repository repo, ArrayList<Manifest> packages) {
        this.repo = repo;
        this.packages = packages;
        app.rule (Method.GET | Method.POST, "/new", accept ("text/html", upload));
        app.get ("/<pkg:id>", accept ("text/html", pkg));
    }

    public bool upload (Request req, Response res) throws Redirection {
        if (req.method == "GET") {
            return res.expand_utf8 (Views.upload ());
        } else {
            var form = Soup.Form.decode (req.flatten_utf8 ());
            pending_packages.add (form["name"]);
            load_package.begin (form["url"], form["name"], (obj, res) => {
                var pack = load_package.end (res);
                pending_packages.remove (form["name"]);
                this.packages.add (pack);
                this.repo.packages[pack.name] = pack.repository;
            });
            throw new Redirection.FOUND ("/%s".printf (form["name"]));
        }
    }

    public bool pkg (Request req, Response res, NextCallback next, Context ctx) throws Error {
        string id = ctx["id"].get_string ();
        foreach (Manifest pkg in packages) {
            if (pkg.name == id) {
                return res.expand_utf8 (template (pkg.name,
                    Compose.HTML5.main ({"class=package"},
                        div ({},
                            h1 ({}, pkg.name),
                            p ({}, pkg.description),
                            pre (get_readme (pkg.name))
                        ),
                        aside ({},
                            div ({"class=pkg-info"},
                                code ({}, "poulp install %s".printf (pkg.name)),
                                p ({},
                                    icon ("arrow-down"),
                                    "%d downloads".printf (Random.int_range (0, 10000))),
                                p ({},
                                    icon ("star"),
                                    "%d stars".printf (Random.int_range (0, 10000))),
                                p ({},
                                    icon ("code"),
                                    a (pkg.repository.replace (".git", ""), {}, pkg.repository.replace ("https://", "").replace ("http://", "").replace (".git", "")))
                            ),
                            p ({},
                                icon ("legal"),
                                pkg.license)
                        )
                    )
                ));
            }
        }
        foreach (var pkg in pending_packages) {
            if (pkg == id) {
                return res.expand_utf8 (template ("Downloading %s".printf (id),
                    p ({}, "We are downloading informations about your package, try to refresh in a few seconds…")
                ));
            }
        }
        throw new ClientError.NOT_FOUND (@"Can't find any package named $id.");
    }
}
