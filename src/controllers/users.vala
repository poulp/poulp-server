using Valum;
using Valum.ContentNegotiation;
using Views;
using VSGI;

public class UserController : Object {

    public UserController.on (Router app) {
        app.get ("/login", accept ("text/html", login));
        app.get ("/@<name>", accept ("text/html", page));
    }

    public bool login (Request req, Response res) throws Error {
        return res.expand_utf8 (Views.login ());
    }

    public bool page (Request req, Response res, NextCallback next, Context ctx) throws Error {
        var usr_name = ctx["name"].get_string ();
        return res.expand_utf8 (template ("@" + usr_name, usr_name));
    }
}
