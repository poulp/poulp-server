using Valum;
using Valum.ContentNegotiation;
using Valum.Static;
using VSGI;

using Compose;
using Compose.HTML5;
using Views;

using Gee;
using Poulp;
using Json;

void main (string[] args) {
    string repo_content;
    FileUtils.get_contents (Path.build_filename ("data", "packages.json"), out repo_content);
    Json.Parser parser = new Json.Parser ();
    parser.load_from_data (repo_content);
    var repo = new Repository.from_json (parser.root.as_object ());
    ArrayList<Manifest> packages = new ArrayList<Manifest> ();

    foreach (var pkg in repo.packages.entries) {
        load_package.begin (pkg.value, pkg.key, (obj, res) => {
            packages.add (load_package.end (res));
        });
    }

    var app = new Router ();
    app.use (basic ());
    app.get ("/static/<path:path>", serve_from_path ("static"));
    app.register_type ("pkg", /[\w-_]+/);

    app.use (status (404, (req, res, next, ctx, err) => {
        res.headers.set_content_type ("text/html", null);
        return res.expand_utf8 (template ("404 Not Found",
            Compose.HTML5.main ({},
                h1 ({}, "404 Not Found"),
                p ({}, err.message)
            )
        ));
    }));

    new UserController.on (app);
    new HomeController.on (app, repo, packages);
    new PackagesController.on (app, repo, packages);

    app.get ("/search", accept ("text/html", (req, res) => {
        var query = req.lookup_query ("q");
        if (query == null || query == "") {
            return res.expand_utf8 (template ("Search",
                Compose.HTML5.main ({},
                    p ({}, "Please enter a (non-empty) search.")
                )
            ));
        } else {
            return res.expand_utf8 (
                template ("Results for %s".printf (query),
                    Compose.HTML5.main ({},
                        for_each<Manifest> (
                            filter<Manifest> (packages, (pkg, c) => {
                                if (c < 10) {
                                    return pkg.name.contains (query);
                                }
                                return null;
                            }),
                            (pkg, i) => {
                                return div ({},
                                    h3 ({}, pkg.name),
                                    p ({"class=light"}, pkg.version.to_string ()),
                                    p ({}, pkg.description)
                                );
                            },
                            p ({},
                                "Sorry, no results for %s, try another search".printf (query)
                            )
                        )
                    )
                )
            );
        }
    }));

    Server.@new ("http", handler: app).run (args);
}

string get_readme (string pkg) {
    string path = Path.build_filename ("data", "packages", pkg, "README");
    if (!FileUtils.test (path, FileTest.EXISTS)) {
        if (FileUtils.test (path + ".md", FileTest.EXISTS)) {
            path += ".md";
        } else {
            return "This package has no README";
        }
    }
    string readme;
    FileUtils.get_contents (path, out readme);
    return readme;
}

async Manifest load_package (string url, string name) {
    var dest = Path.build_filename ("data", "packages", name);
    yield spawn_async ("git clone %s %s".printf (url, dest));
    return new Manifest.for_path (Path.build_filename (dest, "poulp.toml"));
}

SemVer.Version get_app_version () {
    return new SemVer.Version (0, 1, 0);
}
