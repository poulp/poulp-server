using Compose.HTML5;

namespace Views {
    public string icon (string name, bool access_hide = true) {
        return i ({"class=fa fa-%s".printf (name), access_hide ? "aria-hidden=true" : "alt=%s".printf (name)});
    }

    public string template (string title, string content) {
        return html ({},
                head ({},
                    meta ({"charset=utf8"}),
                    Compose.HTML5.title (title + " — Poulp"),
                    link ("/static/css/style.min.css"),
                    link ("/static/css/font-awesome.css"),
                    link ("/static/img/poulp.png", "shortcut icon apple-touch-icon")
                ),
                body ({},
                    header ({},
                        h1 ({}, a ("/", {}, "Poulp")),
                        form ({"method=post", "action=/search"},
                            input ("search", null, {"autocomplete=off"}),
                            a ("/search", {"id=search-submit"}, icon ("search"))
                        ),
                        span ({},
                            a ("/login", {}, "Login")
                        )
                    ),
                    Compose.HTML5.main ({}, content),
                    footer ({},
                            h2 ({}, "Poulp"),
                            div ({"class=links"},
                                p ({}, "Powered by Poulp server %s".printf (get_app_version ().to_string ())),
                                a ("/about", {}, "About"),
                                a ("/api", {}, "API"),
                                a ("/terms", {}, "Terms"),
                                a ("https://framagit.org/poulp/poulp-server", {}, "Contribute to the code")
                            )
                    )
                )
            );
    }
}
