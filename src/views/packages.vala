using Compose.HTML5;

namespace Views {
    public string upload () {
        return template ("New package", Compose.HTML5.main ({},
            h1 ({}, "New package"),
            form ({"method=post"},
                label ("name", {}, "Name of the package"),
                input ("name"),
                label ("url", {}, "Git repository URL"),
                input ("url"),
                input ("submit", "Publish", {"type=submit"})
            )
        ));
    }
}
