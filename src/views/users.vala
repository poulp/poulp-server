using Compose.HTML5;

namespace Views {
    public string login () {
        return template ("Login", Compose.HTML5.main ({},
            h1 ({}, "Login"),
            a ("#", {"class=login gh"},
                icon ("github"),
                "With GitHub"
            ),
            a ("#", {"class=login gl"},
                icon ("gitlab"),
                "With GitLab.com"
            ),
            "More options comming soon!"
        ));
    }
}
